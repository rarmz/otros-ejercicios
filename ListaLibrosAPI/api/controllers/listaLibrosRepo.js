const data = { books: [] };

exports.get = () => {
  return data;
};

exports.add = (book) => {
  data.books.push(book);
};

exports.find = (id) => {
  book = data.books.find((book) => book.id === +id);
  if (book) return book;
  return {};
};

exports.upd = (book) => {
  bookFound = data.books.find((sbook) => sbook.id === +book.id);

  if (bookFound) {
    bookFound.titulo = book.titulo;
    bookFound.autor = book.autor;
  }
};

exports.del = (id) => {
  data.books = data.books.filter((book) => book.id !== +id);
};
