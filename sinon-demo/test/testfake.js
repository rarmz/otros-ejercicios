const assert = require("assert");
const unavez = require("../unavez");
const sinon = require("sinon");

describe("llama a la funcion original", () => {
  let callback = sinon.fake();
  let proxy = unavez.llamar(callback);
  let obj = {};
  proxy.call(obj, 1, 2, 3);
  proxy.call(obj, 1, 2, 4);
  assert(callback.calledOnce);
  assert(callback.calledWith(1, 2, 3));
});

it("Comprobar retorno", () => {
  let callback = sinon.fake.returns(42);
  let proxy = unavez.llamar(callback);
  assert(proxy() === 42);
  assert(proxy() === 42);
});
