const assert = require("assert");
const greeter = require("../greeter");
const sinon = require("sinon");
describe("test de greeter", () => {
  it("checkea greet function", () => {
    // let clock = sinon.useFakeTimers(new Date(2021, 03, 15));
    assert.strictEqual(
      greeter.greet("Ramiro"),
      `Hola, Ramiro! Hoy es Tuesday, April 20, 2021`
    );
  });
});
