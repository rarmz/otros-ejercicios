(function (exports) {
  function greet(name) {
    let options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    let now = new Date();
    let formattedData = now.toLocaleDateString("en-US", options);
    return `Hola, ${name}! Hoy es ${formattedData}`;
  }
  exports.greet = greet;
})(this);
