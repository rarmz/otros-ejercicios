(function (exports) {
  function llamar(fn) {
    let valorRetorno,
      llamada = false;
    return function () {
      if (!llamada) {
        llamada = true;
        valorRetorno = fn.apply(this, arguments);
      }
      return valorRetorno;
    };
  }
  exports.llamar = llamar;
})(this);
