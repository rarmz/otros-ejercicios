import { css, html, LitElement } from "lit-element";

export class TestAjax extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: 1px solid blue;
        margin: 2em;
      }
    `;
  }
  static get properties() {
    return {
      planet: { type: Object },
    };
  }

  constructor() {
    super();
    this.planet = {};
    this.cargarPlaneta();
  }

  render() {
    return html` <p>${this.planet.name}</p> `;
  }

  cargarPlaneta() {
    /**
     * Requiere que en el INDEX se
     * haga referencia a
     * <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     *
     * Libreria jquery
     */
    $.get("https://swapi.dev/api/planets/1", (data, status) => {
      if (status === "success") {
        this.planet = data;
      } else {
        alert("Error al llamar al rest");
      }
    });
  }
}

customElements.define("test-ajax", TestAjax);
