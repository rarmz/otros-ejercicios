import { LitElement, html, css } from "lit-element";
class ProductList extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 2em;
        margin: 2em;
        border: 1px solid brown;
      }
    `;
  }

  static get properties() {
    return {
      productos: { type: Array },
    };
  }
  constructor() {
    super();
    this.productos = [];
    this.productos.push({
      nombre: "Movil XL",
      descripcion: "Telefono con una de las mejores pantallas",
    });
    this.productos.push({
      nombre: "Movil Mini",
      descripcion: "Telefono mediano on una de las mejores camaras",
    });
    this.productos.push({
      nombre: "Movil Standard",
      descripcion: "Telefono estandar. Sin nada especial",
    });
  }

  render() {
    return html`
      <div class="contenedor">
        ${this.productos.map((p) => {
          return html` <div class="producto">
            <h3>${p.nombre}</h3>
            <p>${p.descripcion}</p>
          </div>`;
        })}
      </div>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("products-list", ProductList);
