import { LitElement, html, css } from "lit-element";

export class TestXmlhttp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: 1px solid red;
        margin: 2em;
        overflow-x: scroll;
      }
    `;
  }
  static get properties() {
    return {
      planet: { type: Object },
    };
  }

  constructor() {
    super();
    this.cargarPlaneta();
  }

  render() {
    return html` <p><code> ${this.planet} </code></p> `;
  }

  cargarPlaneta() {
    // xmlHttpRequest
    // Documentacion: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
    let req = new XMLHttpRequest();
    req.open("GET", "https://swapi.dev/api/planets/1", true);
    req.onre;
    req.onreadystatechange = (ev) => {
      if (req.readyState === 4) {
        if (req.status === 200) {
          this.planet = req.responseText;
          this.requestUpdate();
        } else {
          alert("Error llamando al rest");
        }
      }
    };
    req.send(null);
  }
}

customElements.define("test-xmlhttp", TestXmlhttp);
