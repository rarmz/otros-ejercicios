import { html, LitElement, css } from "lit-element";

class OpenTable extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: 1px solid green;
        margin: 2em;
      }
    `;
  }
  static get properties() {
    return {
      resultados: { type: Object },
      campos: { type: Array },
      fila: { type: Array },
      sig: { type: Boolean },
      prev: { type: Boolean },
      entidades: { type: Array },
    };
  }

  constructor() {
    super();
    this.campos = [];
    this.resultados = { results: [] };
    this.filas = [];
    this.entidades = [
      "planets",
      "films",
      "people",
      "species",
      "starships",
      "vehicles",
    ];
    this.cargarDatos("https://swapi.dev/api/planets");
  }

  render() {
    return html`
      <div>
        <select id="sel" @change="${this.changeSelect}">
          ${this.entidades.map((ent) => {
            return html`<option>${ent}</option>`;
          })}
        </select>
      </div>
      <table width="100%">
        <tr>
          ${this.campos.map((campo) => {
            return html`<th>${campo}</th>`;
          })}
        </tr>
        ${this.filas.map((fila) => {
          return html`<tr>
            ${this.resultados.results[fila].valores.map((data) => {
              return html`<td>${data}</td>`;
            })}
          </tr>`;
        })}
      </table>
      ${!this.prev
        ? html``
        : html`<div><button @click="${this.anterior}">Anterior</button></div>`}
      ${!this.sig
        ? html``
        : html`<div>
            <button @click="${this.siguiente}">Siguiente</button>
          </div>`}
    `;
  }

  changeSelect(e) {
    // console.log("e >>>", e.target.value);
    // let entidad = this.shadowRoot.querySelector("#sel").value;
    let newURL = `https://swapi.dev/api/${e.target.value}/`;
    this.cargarDatos(newURL);
  }
  anterior() {
    this.cargarDatos(this.resultados.previous);
  }

  siguiente() {
    this.cargarDatos(this.resultados.next);
  }
  cargarDatos(url) {
    // "https://swapi.dev/api/planets"
    fetch(url)
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        this.resultados = data;
        // console.log(data);
        this.montarResultados();
      })
      .catch((err) => {
        alert(`Something wrong:  ${err}`);
      });
  }

  montarResultados() {
    this.campos = this.getObjProps(this.resultados.results[0]);
    this.filas = [];
    for (let i = 0; i < this.resultados.results.length; i++) {
      this.resultados.results[i].valores = this.getValores(
        this.resultados.results[i]
      );
      this.filas.push(i);
    }
    this.sig = this.resultados.next ? true : false;
    this.prev = this.resultados.previous ? true : false;
  }
  getValores(fila) {
    let valores = [];
    for (let i = 0; i < this.campos.length; i++) {
      valores[i] = fila[this.campos[i]];
    }
    return valores;
  }

  getObjProps(obj) {
    let props = [];
    for (let prop in obj) {
      if (!this.isArray(this.resultados.results[0][prop])) {
        prop !== "created" &&
          prop !== "edited" &&
          prop !== "url" &&
          props.push(prop);
      }
    }

    return props;
  }

  isArray(prop) {
    // return Object.prototype.toString.call(prop) == "[object Array]";
    return Array.isArray(prop);
  }
}

customElements.define("open-table", OpenTable);
