import { LitElement, html, css } from "lit-element";

export class TestFetch extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: 1px solid yellow;
        margin: 2em;
        overflow-x: scroll;
      }
    `;
  }
  static get properties() {
    return {
      planets: { type: Object },
    };
  }

  constructor() {
    // Permite inicializar propiedades
    super();
    this.planets = { results: [] };
    // this.cargarPlaneta();
  }

  render() {
    return html`
      ${this.planets.results.map((planet) => {
        return html`<div>${planet.name} - ${planet.rotation_period}</div> `;
      })}
    `;
  }

  connectedCallback() {
    /***
     * Se ejecuta apenas se termina de cargar
     * por completo el componente
     */
    super.connectedCallback();
    try {
      this.cargarPlaneta();
    } catch (e) {
      alert(e);
    }
  }
  cargarPlaneta() {
    fetch("https://swapi.dev/api/planets")
      .then((res) => {
        // console.log(res);
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        //this.planets.results = data.results;
        this.planets = data;
        // console.log(this.planets.results);
        //console.log(data);
        // this.requestUpdate();
      })
      .catch((err) => alert(err));
  }
}

customElements.define("test-fetch", TestFetch);
