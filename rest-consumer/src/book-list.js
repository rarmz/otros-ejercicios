import { LitElement, html, css } from "lit-element";
class BookList extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: 1px solid gray;
        margin: 2em;
        padding: 2em;
      }
    `;
  }
  static get properties() {
    return {
      data: { type: Object },
    };
  }
  constructor() {
    super();
    this.data = { books: [] };
    this.cargarDatos();
  }

  render() {
    return html`
      ${this.data.books.map((book) => {
        return html`
          <div><strong>${book.titulo}</strong> - ${book.autor}</div>
        `;
      })}
    `;
  }

  cargarDatos() {
    fetch("http://localhost:3392/books/")
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        this.data = data;
      })
      .catch((err) => alert(err));
  }
}

customElements.define("book-list", BookList);
