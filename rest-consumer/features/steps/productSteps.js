// import { When, Then, After } from "@cucumber/cucumber";
const { When, Then, After } = require("@cucumber/cucumber");
// import assert from "assert";
const assert = require("assert");
// import { Builder, By, until } from "selenium-webdriver";
const { Builder, By, until } = require("selenium-webdriver");
// import chrome from "selenium-webdriver/chrome";
const chrome = require("selenium-webdriver/chrome");
// import chromedriver from "chromedriver";
const chromedriver = require("chromedriver");

When("we request the products list", async () => {
  chrome.setDefaultService(
    new chrome.ServiceBuilder(chromedriver.path).build()
  );
  this.driver = new Builder().forBrowser("chrome").build();
  this.driver.wait(until.elementLocated(By.className("products-list")));
  await this.driver.get("http://localhost:8000/");
});

Then("we should receive", async (dataTable) => {
  let productElements = await this.driver.findElements(
    By.className("producto")
  );

  let expectations = dataTable.hashes();
  for (let index = 0; index < expectations.length; index++) {
    const productName = await productElements[index]
      .findElement(By.tagName("h3"))
      .getText();
    assert.strictEqual(productName, expectations[index].nombre);

    const productDesc = await productElements[index]
      .findElement(By.tagName("p"))
      .getText();
    assert.strictEqual(productDesc, expectations[index].description);
  }
});

// Para que cerremos el driver
After(async () => {
  this.driver.close();
});
