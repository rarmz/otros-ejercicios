const { Given, When, Then } = require("@cucumber/cucumber");
const assert = require("assert");

let a, b, t;

Given("numbers {int} and {int}", (i1, i2) => {
  //   [a,b] = [i1, i2];
  a = i1;
  b = i2;
});

When("they are added togheter", () => {
  t = a + b;
});

Then("the total should be {int}", (expected) => {
  assert.strictEqual(t, expected);
});
